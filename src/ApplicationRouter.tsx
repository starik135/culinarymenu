import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import RecipesDishes from './components/pages/RecipesDishes';
import ListDishes from './components/pages/listDishes';
import Ingredients from './components/pages/Ingredients';
import OrderIngredients from './components/pages/OrderIngredients';

import {
  HOME_APP_ENDPOINT,
  LIST_DISHES_ENDPOINT,
  INGREDIENTS_ENDPOINT,
  ONLINE_STORE_ENDPOINT,
} from './constants/index';

export default class ApplicationRouter extends React.Component {

  render() {
    return (
      <Router>
        <div className="application-wrapper">
          <Route path={HOME_APP_ENDPOINT} component={RecipesDishes} exact />
          <Route path={LIST_DISHES_ENDPOINT} component={ListDishes} />
          <Route path={INGREDIENTS_ENDPOINT} component={Ingredients} />
          <Route path={ONLINE_STORE_ENDPOINT} component={OrderIngredients} />
        </div>
      </Router>
    );
  }
}
