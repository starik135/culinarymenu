import { Action } from 'redux';

export interface IErrorProps {
  error: string;
}

// @todo: fix object | []
export const createActionObject = (type: string, props?: Object | []): Action => {
  return {
    type,
    ...props,
  };
};
