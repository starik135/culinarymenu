import { createActionObject, IErrorProps } from '../../actions';

export const SET_ORDER_FRENCH_REQUEST = '@/SET_ORDER_FRENCH_REQUEST';
export const SET_ORDER_FRENCH_RESPONSE = '@/SET_ORDER_FRENCH_RESPONSE';
export const SET_ORDER_FRENCH_FAIL = '@/SET_ORDER_FRENCH_FAIL';

export const setOrderFrenchRequest = () => {
  return createActionObject(SET_ORDER_FRENCH_REQUEST);
};

export const setOrderFrenchResponse = (payload: any) => {
  return createActionObject(SET_ORDER_FRENCH_RESPONSE, { payload });
};

export const setOrderFrenchFail = (error: IErrorProps) => {
  return createActionObject(SET_ORDER_FRENCH_FAIL, { ...error });
};
