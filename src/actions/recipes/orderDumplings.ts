import { createActionObject, IErrorProps } from '../../actions';

export const SET_ORDER_DUMPLINGS_REQUEST = '@/SET_ORDER_DUMPLINGS_REQUEST';
export const SET_ORDER_DUMPLINGS_RESPONSE = '@/SET_ORDER_DUMPLINGS_RESPONSE';
export const SET_ORDER_DUMPLINGS_FAIL = '@/SET_ORDER_DUMPLINGS_FAIL';

export const setOrderDumplingsRequest = () => {
  return createActionObject(SET_ORDER_DUMPLINGS_REQUEST);
};

export const setOrderDumplingsResponse = (payload: any) => {
  return createActionObject(SET_ORDER_DUMPLINGS_RESPONSE, { payload });
};

export const setOrderDumplingsFail = (error: IErrorProps) => {
  return createActionObject(SET_ORDER_DUMPLINGS_FAIL, { ...error });
};
