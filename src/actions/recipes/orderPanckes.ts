import { createActionObject, IErrorProps } from '../../actions';

export const SET_ORDER_PANCKES_REQUEST = '@/SET_ORDER_PANCKES_REQUEST';
export const SET_ORDER_PANCKES_RESPONSE = '@/SET_ORDER_PANCKES_RESPONSE';
export const SET_ORDER_PANCKES_FAIL = '@/SET_ORDER_PANCKES_FAIL';

export const setOrderPanckesRequest = () => {
  return createActionObject(SET_ORDER_PANCKES_REQUEST);
};

export const setOrderPanckesResponse = (payload: any) => {
  return createActionObject(SET_ORDER_PANCKES_RESPONSE, { payload });
};

export const setOrderPanckesFail = (error: IErrorProps) => {
  return createActionObject(SET_ORDER_PANCKES_FAIL, { ...error });
};
