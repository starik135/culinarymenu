import { createActionObject, IErrorProps } from '../../actions';

export const GET_RECIPES_REQUEST = '@/GET_RECIPES_REQUEST';
export const GET_RECIPES_RESPONSE = '@/GET_RECIPES_RESPONSE';
export const GET_RECIPES_FAIL = '@/GET_RECIPES_FAIL';

export const getRecipesRequest = () => {
  return createActionObject(GET_RECIPES_REQUEST);
};

export const getRecipesResponse = (payload: any) => {
  return createActionObject(GET_RECIPES_RESPONSE, { payload });
};

export const getRecipesFail = (error: IErrorProps) => {
  return createActionObject(GET_RECIPES_FAIL, { ...error });
};
