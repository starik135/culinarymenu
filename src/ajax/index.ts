import axios from 'axios';

import { AUTH_COOKIE_NAME } from '../constants';

const cookie = require('react-cookies');

export const objectToQueryString = (queryObject: {}): string => {
  return Object.keys(queryObject)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(queryObject[key])}`)
    .join('&');
};

export const generateCorsHeader = () => ({
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
});

export const generateBearerAuthHeaderFromCookie = () => {
  const token = cookie.load(AUTH_COOKIE_NAME);

  if (token && token.access) {
    return {
      Authorization: `Bearer ${token.access}`,
    };
  }

  return {};
};

// tslint:disable-next-line: no-any
export const sendRequest = (params: any): Promise<any> => {
  const { method, url, body, query, headers } = params;
  const queryString = query && Object.keys(query).length > 0 && objectToQueryString(query);

  if (!method) {
    throw new Error('Method could not be empty, please check arguments of sendRequest function');
  }

  if (!url) {
    throw new Error('Url could not be empty, please check arguments of sendRequest function');
  }

  const requestArguments = {
    method,
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
  };

  queryString && queryString.length > 0
    ? (requestArguments['url'] = `${url}?${queryString}`)
    : (requestArguments['url'] = url);

  body && (requestArguments['data'] = body);

  return axios(requestArguments);
};
