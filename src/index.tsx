import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './store';
import ApplicationRouter from './ApplicationRouter';

export default class Application extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ApplicationRouter />
      </Provider>
    );
  }
}

ReactDOM.render(
  <Application />,
  document.getElementById('root'),
);
