import { AnyAction } from 'redux';
import { takeEvery, put, call, select } from 'redux-saga/effects';

import { getState } from '../selectors';
import { sendRequest, generateBearerAuthHeaderFromCookie, generateCorsHeader } from '../../ajax';
import {
  setOrderDumplingsResponse,
  setOrderDumplingsFail,
  SET_ORDER_DUMPLINGS_REQUEST,
} from '../../actions/recipes';

import {
  RequestType, SET_ORDER_DUMPLINGS_ENDPOINT,
} from '../../constants';
import { formValueSelector } from 'redux-form';

function* setOrderDumplings(action: AnyAction) {
  try {
    const loginSelector = formValueSelector('addDumplingsForm');
    const state = yield select(getState);

    const wheatFlour = loginSelector(state, 'wheatFlour');
    const water = loginSelector(state, 'water');
    const salt = loginSelector(state, 'salt');
    const mincedMeat = loginSelector(state, 'mincedMeat');
    const onions = loginSelector(state, 'onions');
    const blackPepper = loginSelector(state, 'blackPepper');

    const response = yield call(sendRequest, {
      method: RequestType.POST,
      url: SET_ORDER_DUMPLINGS_ENDPOINT,
      headers: {
        ...generateBearerAuthHeaderFromCookie(),
        ...generateCorsHeader(),
      },
      body: {
        wheatFlour,
        water,
        salt,
        mincedMeat,
        onions,
        blackPepper,
      },
    });

    yield put(setOrderDumplingsResponse(response));
  } catch (error) {
    yield put(setOrderDumplingsFail(error));
  }
}

export function* setOrderDumplingsSaga() {
  yield takeEvery(SET_ORDER_DUMPLINGS_REQUEST, setOrderDumplings);
}
