import { AnyAction } from 'redux';
import { takeEvery, put, call, select } from 'redux-saga/effects';

import { sendRequest, generateBearerAuthHeaderFromCookie, generateCorsHeader } from '../../ajax';
import {
  setOrderFrenchResponse,
  setOrderFrenchFail,
  SET_ORDER_FRENCH_REQUEST,
} from '../../actions/recipes';

import {
  RequestType, SET_ORDER_FRENCH_ENDPOINT,
} from '../../constants';
import { formValueSelector } from 'redux-form';
import { getState } from '../selectors';

function* setOrderFrench(action: AnyAction) {
  try {
    const loginSelector = formValueSelector('addFrenchFriesForm');
    const state = yield select(getState);

    const potatoes = loginSelector(state, 'potatoes');
    const vegetableOil = loginSelector(state, 'vegetableOil');
    const salt = loginSelector(state, 'salt');

    const response = yield call(sendRequest, {
      method: RequestType.POST,
      url: SET_ORDER_FRENCH_ENDPOINT,
      headers: {
        ...generateBearerAuthHeaderFromCookie(),
        ...generateCorsHeader(),
      },
      body: {
        potatoes,
        vegetableOil,
        salt,
      },
    });

    yield put(setOrderFrenchResponse(response));
  } catch (error) {
    yield put(setOrderFrenchFail(error));
  }
}

export function* setOrderFrenchSaga() {
  yield takeEvery(SET_ORDER_FRENCH_REQUEST, setOrderFrench);
}
