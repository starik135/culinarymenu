import { AnyAction } from 'redux';
import { takeEvery, put, call } from 'redux-saga/effects';

import { sendRequest, generateBearerAuthHeaderFromCookie, generateCorsHeader } from '../../ajax';
import { getRecipesResponse, getRecipesFail, GET_RECIPES_REQUEST } from '../../actions/recipes';

import {
  RequestType, RECIPES_ENDPOINT,
} from '../../constants';

function* getRecipes(action: AnyAction) {
  try {
    const response = yield call(sendRequest, {
      method: RequestType.GET,
      url: RECIPES_ENDPOINT,
      headers: {
        ...generateBearerAuthHeaderFromCookie(),
        ...generateCorsHeader(),
      },
    });

    yield put(getRecipesResponse(response));
  } catch (error) {
    yield put(getRecipesFail(error));
  }
}

export function* getRecipesSaga() {
  yield takeEvery(GET_RECIPES_REQUEST, getRecipes);
}
