import { AnyAction } from 'redux';
import { takeEvery, put, call, select } from 'redux-saga/effects';

import { sendRequest, generateBearerAuthHeaderFromCookie, generateCorsHeader } from '../../ajax';
import {
  setOrderPanckesResponse,
  setOrderPanckesFail,
  SET_ORDER_PANCKES_REQUEST,
} from '../../actions/recipes';

import {
  RequestType, SET_ORDER_PANCKES_ENDPOINT,
} from '../../constants';
import { formValueSelector } from 'redux-form';
import { getState } from '../selectors';

function* setOrderPanckes(action: AnyAction) {
  try {
    const loginSelector = formValueSelector('addPancakesForm');
    const state = yield select(getState);

    const potatoes = loginSelector(state, 'potatoes');
    const vegetableOil = loginSelector(state, 'vegetableOil');
    const salt = loginSelector(state, 'salt');
    const onion = loginSelector(state, 'onion');

    const response = yield call(sendRequest, {
      method: RequestType.POST,
      url: SET_ORDER_PANCKES_ENDPOINT,
      headers: {
        ...generateBearerAuthHeaderFromCookie(),
        ...generateCorsHeader(),
      },
      body: {
        potatoes,
        vegetableOil,
        salt,
        onion,
      },
    });

    yield put(setOrderPanckesResponse(response));
  } catch (error) {
    yield put(setOrderPanckesFail(error));
  }
}

export function* setOrderPanckesSaga() {
  yield takeEvery(SET_ORDER_PANCKES_REQUEST, setOrderPanckes);
}
