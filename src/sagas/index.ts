import { fork, all } from 'redux-saga/effects';

import * as recipes from './recipes';

function* rootSaga() {
  yield all([
    fork(recipes.getRecipesSaga),
    fork(recipes.setOrderDumplingsSaga),
    fork(recipes.setOrderFrenchSaga),
    fork(recipes.setOrderPanckesSaga),
  ]);
}

export default rootSaga;
