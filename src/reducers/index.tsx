import { combineReducers, Reducer } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { routerReducer } from 'react-router-redux';
import recipesReducer from './recipes';

export const reducers: Reducer<any> = combineReducers<any>({
  form: formReducer,
  router: routerReducer,
  recipes: recipesReducer,
});
