import { AnyAction } from 'redux';

import { RequestStatus, CLEAR_USER_DATA } from '../../constants';
import {
  GET_RECIPES_REQUEST,
  GET_RECIPES_RESPONSE,
  GET_RECIPES_FAIL,
} from '../../actions/recipes';

const initialState: Object = {
  data: {},
  request: {
    errorText: '',
    status: '',
  },
};

export default function reducer(state: Object = initialState, action: AnyAction) {
  switch (action.type) {
    case GET_RECIPES_REQUEST:
      return Object.assign({}, state, {
        request: {
          status: RequestStatus.PENDING,
        },
      });

    case GET_RECIPES_RESPONSE:
      return Object.assign({}, state, {
        data: action.payload.data,
        request: {
          status: RequestStatus.SUCCESS,
        },
      });

    case GET_RECIPES_FAIL:
      return Object.assign({}, state, {
        data: {},
        request: {
          status: RequestStatus.ERROR,
          errorText: action.error,
        },
      });

    case CLEAR_USER_DATA:
      return { ...initialState };

    default:
      return state;
  }
}
