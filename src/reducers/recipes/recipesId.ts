import { AnyAction } from 'redux';

import { CLEAR_USER_DATA, SET_RECIPES_ID } from '../../constants';

const initialState: any = 0;

export default function reducer(state: any = initialState, action: AnyAction) {
  switch (action.type) {
    case SET_RECIPES_ID:
      return action.id;

    case CLEAR_USER_DATA:
      return { initialState };

    default:
      return state;
  }
}
