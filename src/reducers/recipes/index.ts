import { combineReducers } from 'redux';

import recipesReducer from './recipes';
import recipesIdReducer from './recipesId';

export default combineReducers<any>({
  recipes: recipesReducer,
  recipesId: recipesIdReducer,
});
