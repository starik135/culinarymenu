import * as React from 'react';

import './styles';

export const DishesTypes = (props: any) => (
  <React.Fragment>
    <button onClick={props.X} >Назад</button>
    {
      props.items.map((item: any, index: number) => (
        <div key={index} className="pancakes">
          <div className="pancakes-img">
            <img src={item.image} />
          </div>
          <div className="pancakes-info">
            <p>{item.title}</p>
            <button onClick={props.openOrder(index)}>Order ingredient</button>
            <button onClick={props.openRecipes(index)}>Read More</button>
          </div>
        </div>
      ))
    }
  </React.Fragment>
);
