import * as React from 'react';

import Visibility from '@material-ui/icons/Visibility';

import { TextField, IconButton, FormHelperText } from '@material-ui/core';

import { FIELD_TYPE_TEXT } from '../../../constants';

import {
  AllTextInputProps,
  IComponentState,
} from './types';

import './styles.scss';

export default class TextInputField extends React.Component<AllTextInputProps, IComponentState> {
  constructor(props: AllTextInputProps) {
    super(props);

    this.state = {
      isVisible: false,
    };
  }

  private handleClickShowPassword = () => {
    this.setState({ isVisible: !this.state.isVisible });
  }

  render() {
    const { className, input, label, disabled, meta, iconEye } = this.props;

    return (
      <div className={className}>
        <TextField
          {...input}
          label={label}
          type={FIELD_TYPE_TEXT}
          disabled={disabled}
          error={meta.touched && !!meta.error}
        />
        {
          meta.touched
            && !!meta.error
            && <FormHelperText style={{ color: '#f44336' }}>{meta.error}</FormHelperText>
        }
        {
          iconEye
            && <IconButton onClick={this.handleClickShowPassword}>
              <Visibility />
            </IconButton>
        }
      </div>
    );
  }
}
