import { WrappedFieldProps } from 'redux-form';

interface ITextInputProps {
  className?: string;
  label: string;
  type: string;
  value: string;
  disabled: boolean;
  iconEye: boolean;
  onHandleChange: (value: React.KeyboardEventHandler) => void;
}

export interface IComponentState {
  isVisible: boolean;
}

export type AllTextInputProps = ITextInputProps & WrappedFieldProps;
