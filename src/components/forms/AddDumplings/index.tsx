import * as React from 'react';

import { compose } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';

import { TextInputField } from '../../fields';

import { FIELD_TYPE_TEXT } from '../../../constants';

import {
  AllProps,
  IComponentProps,
} from './types';

import './styles';

const AddDumplingsForm = (props: AllProps) => (
  <form onSubmit={props.handleSubmit} className="eeee">
  <p className="headline">Count ingredients</p>
    <Field
      type={FIELD_TYPE_TEXT}
      id={'wheatFlour'}
      name={'wheatFlour'}
      component={TextInputField}
      label="Wheat flour"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'water'}
      name={'water'}
      component={TextInputField}
      label="Water"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'salt'}
      name={'salt'}
      component={TextInputField}
      label="Salt"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'mincedMeat'}
      name={'mincedMeat'}
      component={TextInputField}
      label="Minced meat"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'onions'}
      name={'onions'}
      component={TextInputField}
      label="Onions"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'blackPepper'}
      name={'blackPepper'}
      component={TextInputField}
      label="Black pepper"
      className={''}
    />
    <div className="divButton">
      <Button
        variant="contained"
        color="primary"
        type="submit"
      >
        send data
      </Button>
    </div>
  </form>
);

const validate = (values: any) => {
  const errors = {};

  return errors;
};

const formSettings = {
  validate,
  form: 'addDumplingsForm',
};

export default compose<React.ComponentType<IComponentProps>>(
  withRouter,
  reduxForm(formSettings),
)(AddDumplingsForm);
