import AddPancakesForm from './AddPancakes';
import AddFrenchFriesForm from './AddFrenchFries';
import AddDumplingsForm from './AddDumplings';

export {
  AddPancakesForm,
  AddFrenchFriesForm,
  AddDumplingsForm,
};
