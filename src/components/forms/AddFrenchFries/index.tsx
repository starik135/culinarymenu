import * as React from 'react';

import { compose } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';

import { TextInputField } from '../../fields';

import { FIELD_TYPE_TEXT } from '../../../constants';

import {
  AllProps,
  IComponentProps,
} from './types';

import './styles';

const AddFrenchFriesForm = (props: AllProps) => (
  <form onSubmit={props.handleSubmit} className="ewq">
  <p className="headline">Count ingredients</p>
    <Field
      type={FIELD_TYPE_TEXT}
      id={'potatoes'}
      name={'potatoes'}
      component={TextInputField}
      label="Potatoes"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'vegetableOil'}
      name={'vegetableOil'}
      component={TextInputField}
      label="Vegetable oil"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'salt'}
      name={'salt'}
      component={TextInputField}
      label="Salt"
      className={''}
    />
    <div className="divButton">
      <button color="primary" type="submit">
        send data
      </button>
    </div>
  </form>
);

const validate = (values: any) => {
  const errors = {};

  return errors;
};

const formSettings = {
  validate,
  form: 'addFrenchFriesForm',
};

export default compose<React.ComponentType<IComponentProps>>(
  withRouter,
  reduxForm(formSettings),
)(AddFrenchFriesForm);
