export interface IComponentProps {
  handleSubmit: (data: React.FormEvent<HTMLFormElement>) => void;
}

export interface IComponentState {}

export interface IMapStateProps {}

export interface IMapDispatchProps {}

export type AllProps = IComponentProps
  & IMapStateProps
  & IMapDispatchProps;
