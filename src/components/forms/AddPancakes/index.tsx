import * as React from 'react';

import { compose } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';

import { TextInputField } from '../../fields';

import { FIELD_TYPE_TEXT } from '../../../constants';

import {
  AllProps,
  IComponentProps,
} from './types';

import './styles';

const AddPancakesForm = (props: AllProps) => (
  <form onSubmit={props.handleSubmit} className="qwe">
    <p className="headline">Count ingredients</p>
    <Field
      type={FIELD_TYPE_TEXT}
      id={'potatoes'}
      name={'potatoes'}
      component={TextInputField}
      label="Potatoes"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'onion'}
      name={'onion'}
      component={TextInputField}
      label="Onion"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'vegetableOil'}
      name={'vegetableOil'}
      component={TextInputField}
      label="Vegetable oil"
      className={''}
    />
    <Field
      type={FIELD_TYPE_TEXT}
      id={'salt'}
      name={'salt'}
      component={TextInputField}
      label="Salt"
      className={''}
    />
    <div className="divButton">
      <button color="primary" type="submit">
        Send data
      </button>
    </div>
  </form>
);

const validate = (values: any) => {
  const errors = {};

  return errors;
};

const formSettings = {
  validate,
  form: 'addPancakesForm',
  enableReinitialize: true,
};

export default compose<React.ComponentType<IComponentProps>>(
  withRouter,
  reduxForm(formSettings),
)(AddPancakesForm);
