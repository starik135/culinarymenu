import * as React from 'react';
import { AddPancakesForm, AddFrenchFriesForm, AddDumplingsForm } from '../../forms';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose, Dispatch } from 'redux';
import './styles.scss';
import {
  setOrderDumplingsRequest,
  setOrderFrenchRequest,
  setOrderPanckesRequest,
} from '../../../actions/recipes';



class OrderIngredients extends React.Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      id: this.props.recipesId,
    };
  }

  private onHandlePanckes = (event: any) => {
    event.preventDefault();
    this.props.onSetOrderPanckes();
  }

  private onHandleFrench = (event: any) => {
    event.preventDefault();
    this.props.onSetOrderFrench();
  }

  private onHandleDumplings = (event: any) => {
    event.preventDefault();
    this.props.onSetOrderDumplings();
  }

  render() {
    const { recipesId } = this.props;

    return (
      <div>
        {recipesId === 0 && <AddPancakesForm handleSubmit={this.onHandlePanckes} />}
        {recipesId === 1 && <AddFrenchFriesForm handleSubmit={this.onHandleFrench} />}
        {recipesId === 2 && <AddDumplingsForm handleSubmit={this.onHandleDumplings} />}
      </div>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  recipesId: state.recipes.recipesId,
});
const mapDispatchToProps = (dispatch: Dispatch<any>): any => ({
  onSetOrderDumplings: () => dispatch(setOrderDumplingsRequest()),
  onSetOrderFrench: () => dispatch(setOrderFrenchRequest()),
  onSetOrderPanckes: () => dispatch(setOrderPanckesRequest()),
});

export default compose(
  withRouter,
  connect<any, any>(mapStateToProps, mapDispatchToProps),
)(OrderIngredients);
