import * as React from 'react';

import { connect } from 'react-redux';
import { Dispatch, compose } from 'redux';

import { dishesTypes } from '../RecipesDishes/mock';
import { DishesTypes } from '../../blocks/DishesTypes';
import {
  SET_RECIPES_ID,
  INGREDIENTS_ENDPOINT,
  HOME_APP_ENDPOINT,
  ONLINE_STORE_ENDPOINT,
} from '../../../constants';
import { withRouter } from 'react-router-dom';

import '../../../components/blocks/DishesTypes/styles.scss';

export class ListDishes extends React.Component<any, any> {
  private openRecipes = (id: number) => () => {
    this.props.onSetRecipes(id);
    this.props.history.push(INGREDIENTS_ENDPOINT);
  }

  private onOpenOrder = (id: number) => () =>  {
    this.props.onSetRecipes(id);
    this.props.history.push(ONLINE_STORE_ENDPOINT);
  }

  private onClosePage = () => this.props.history.push(HOME_APP_ENDPOINT);

  render() {
    return (
      <div>
        <DishesTypes
          items={dishesTypes}
          openRecipes={this.openRecipes}
          X={this.onClosePage}
          openOrder={this.onOpenOrder}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: any): any => ({});
const mapDispatchToProps = (dispatch: Dispatch<any>): any => ({
  onSetRecipes: (id: number) => dispatch(dispatch({ id, type: SET_RECIPES_ID })),
});

export default compose(
  withRouter,
  connect<any, any>(mapStateToProps, mapDispatchToProps),
)(ListDishes);
