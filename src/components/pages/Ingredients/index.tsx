import * as React from 'react';

import { connect } from 'react-redux';
import { Dispatch, compose } from 'redux';
import { withRouter } from 'react-router-dom';

import { allIngredients } from './mock';

import './styles.scss';
import { LIST_DISHES_ENDPOINT } from '../../../constants';

class Ingredients extends React.Component<any, any> {

  private onClosePage = () => this.props.history.push(LIST_DISHES_ENDPOINT);

  render() {
    const newData = allIngredients.filter((item: any) => item.id === this.props.recipesId);

    return (
      <div className="dishCook">
        <p className="nameDish">{newData[0].title}</p>
        <div className="close">
          <img src={newData[0].src} />
          <p onClick={this.onClosePage} className="krest"> X </p>
        </div>
        {
          newData[0].ingredients.map((item: any, index: number) => (
            <div key={index} className="ingredients">
              <p className="nameIngredients">{item.name}</p>
              <p className="count">{item.count}</p>
            </div>
          ))

        }
      </div>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  recipesId: state.recipes.recipesId,
});
const mapDispatchToProps = (dispatch: Dispatch<any>): any => ({});

export default compose(
  withRouter,
  connect<any, any>(mapStateToProps, mapDispatchToProps),
)(Ingredients);
