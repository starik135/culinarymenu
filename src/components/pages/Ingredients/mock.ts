
export const allIngredients = [
  {
    id: 0,
    title: 'Pancakes',
    src: 'http://miptstream.ru/wp-content/uploads/2017/10/kartoshka.jpg',
    ingredients: [
      {
        name: 'Potatoes',
        count: 12,
      },
      {
        name: 'Onion',
        count: 1,
      },
      {
        name: 'Vegetable oil',
        count: 1,
      },
      {
        name: 'Salt',
        count: 1,
      },
    ],
  },
  {
    id: 1,
    title: 'French fries',
    src: 'https://med.vesti.ru/wp-content/uploads/2017/06/shutterstock_396834889.jpg',
    ingredients: [
      {
        name: 'Potatoes',
        count: 1000,
      },
      {
        name: 'Vegetable oil',
        count: 400,
      },
      {
        name: 'Salt',
        count: 1,
      },
    ],
  },
  {
    id: 2,
    title: 'Dumplings',
    // tslint:disable-next-line: max-line-length
    src: 'https://cdn.tveda.ru/thumbs/e3f/e3f37c34f7219ae35d7c01f229d31d31/5bb91f34de15f11ad2547708cb84d9da.jpg',
    ingredients: [
      {
        name: 'Wheat flour',
        count: 600,
      },
      {
        name: 'Water',
        count: 200,
      },
      {
        name: 'Salt',
        count: 1,
      },
      {
        name: 'Minced meat',
        count: 350,
      },
      {
        name: 'Onions',
        count: 2,
      },
      {
        name: 'Black pepper',
        count: 1,
      },
    ],
  },
];
