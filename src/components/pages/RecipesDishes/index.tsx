import * as React from 'react';

import { connect } from 'react-redux';
import { Carousel } from 'react-responsive-carousel';
import { Dispatch, AnyAction, compose } from 'redux';
import { withRouter } from 'react-router';

import { getRecipesRequest } from '../../../actions/recipes';
import { content, iconItems, headerItems } from './mock';

import {
  AllProps,
  ComponentState,
  MapStateProps,
  MapDispatchProps,
} from './types';

import './styles.scss';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const randomString = require('randomid');

class RecipesDishes extends React.Component<AllProps, ComponentState> {

  componentWillMount() {
    this.props.onGetRecipes();
  }

  private redirectTo = (url: string) => () => this.props.history.push(url);

  private renderHeader = () => (
    <header className="header">
      <h3>Кулинарная книга</h3>
      <span>
        {
         headerItems.map((item: any) => (
          <p className="cursor"
          key={randomString()}
          onClick={this.redirectTo(item.url)}
          >
          {item.title}
          </p>
         ))
        }
      </span>
    </header>
  )

  private renderFooter = () => (
    <footer className="footer">
      <div className = "footerIcon">
        {
          iconItems.map((item: any) => <p key={randomString()}>{item}</p>)
        }
      </div>
      <span>Website template designed (c) 2019</span>
    </footer>
  )

  private renderContent = () => (
    <div className="content">
      <Carousel>
        {
          content.map(item => (
            <div key={randomString()}>
              <img src={item.src} />
              <h2>{item.title}</h2>
              <div className="description">{item.description}</div>
            </div>
          ))
        }
      </Carousel>
    </div>
  )

  public render(): JSX.Element {
    return (
      <div>
        {this.renderHeader()}
        {this.renderContent()}
        {this.renderFooter()}
      </div>
    );
  }
}

const mapStateToProps = (state: any): MapStateProps => ({});
const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): MapDispatchProps => ({
  onGetRecipes: () => dispatch(getRecipesRequest()),
});

export default compose(
  withRouter,
  connect<MapStateProps, MapDispatchProps>(mapStateToProps, mapDispatchToProps),
)(RecipesDishes);
