import * as React from 'react';

import './styles';
import {
  LIST_DISHES_ENDPOINT,
  INGREDIENTS_ENDPOINT,
  ONLINE_STORE_ENDPOINT,
  BANK_ENDPOINT,
} from '../../../constants';

export const headerItems: object[] = [
  {
    title: 'recipe dishes',
    url: LIST_DISHES_ENDPOINT,
  },
  {
    title: 'ingredients catalogue',
    url: INGREDIENTS_ENDPOINT,
  },
  {
    title: 'online store',
    url: ONLINE_STORE_ENDPOINT,
  },
  {
    title: 'bank',
    url: BANK_ENDPOINT,
  },
];

export const iconItems = [
  <i className="fas fa-band-aid"></i>,
  <i className="fas fa-cat"></i>,
  <i className="fas fa-dragon"></i>,
  <i className="far fa-clock"></i>,
  <i className="fas fa-clock"></i>,
];

export const content = [
  {
    title: 'Pancakes',
    description: 'Pancakes description',
    src: 'https://cdn.lifehacker.ru/wp-content/uploads/2018/05/Draniki_1527664015-1140x570.jpg',
  },
  {
    title: 'French fries',
    description: 'French fries description',
    src: 'https://www.seriouseats.com/2018/04/20180309-french-fries-vicky-wasik-15-1500x1125.jpg',
  },
  {
    title: 'Dumplings',
    description: 'Dumplings description',
    src: 'https://images-gmi-pmc.edge-generalmills.com/6059b986-4800-4508-8546-40cb56e3d815.jpg',
  },
];

export const dishesTypes = [
  {
    id: 0,
    title: 'Pancakes',
    image: 'https://cdn.lifehacker.ru/wp-content/uploads/2018/05/Draniki_1527664015-1140x570.jpg',
    text: 'Pancakes description',
  },
  {
    id: 1,
    title: 'French fries',
    image: 'https://www.seriouseats.com/2018/04/20180309-french-fries-vicky-wasik-15-1500x1125.jpg',
    text: 'French fries description',
  },
  {
    id: 2,
    title: 'Dumplings',
    image: 'https://images-gmi-pmc.edge-generalmills.com/6059b986-4800-4508-8546-40cb56e3d815.jpg',
    text: 'Dumplings description',
  },
];
