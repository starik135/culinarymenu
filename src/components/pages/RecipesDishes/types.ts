import { RouteComponentProps } from 'react-router-dom';

export interface ComponentProps {}

export interface ComponentState {}

export interface MapStateProps {}

export interface MapDispatchProps {
  onGetRecipes: () => void;
}

export type AllProps = ComponentProps & MapStateProps & MapDispatchProps & RouteComponentProps<{}>;
