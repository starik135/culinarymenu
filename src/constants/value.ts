export const COOKIE_PREFIX = 'forum';
export const AUTH_COOKIE_NAME = `${COOKIE_PREFIX}-authentication`;

export const RequestStatus = {
  PENDING: 'PENDING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export const RequestType = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete',
  PATCH: 'patch',
};

export const CLEAR_USER_DATA = '@/CLEAR_USER_DATA';
export const SET_RECIPES_ID = '@/SET_RECIPES_ID';

export const FIELD_TYPE_TEXT = 'text';
