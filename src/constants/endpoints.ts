export const BASE_URL = 'http://localhost:3000';

export const RECIPES_ENDPOINT = `${BASE_URL}/recipes`;
export const SET_ORDER_DUMPLINGS_ENDPOINT = `${BASE_URL}/set-order-dumplings`;
export const SET_ORDER_FRENCH_ENDPOINT = `${BASE_URL}/set-order-french`;
export const SET_ORDER_PANCKES_ENDPOINT = `${BASE_URL}/set-order-panckes`;
