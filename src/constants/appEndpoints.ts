export const HOME_APP_ENDPOINT = '/';
export const LIST_DISHES_ENDPOINT = '/dishes';
export const INGREDIENTS_ENDPOINT = '/ingredients';
export const ONLINE_STORE_ENDPOINT = '/online-store';
export const BANK_ENDPOINT = '/bank';
