import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { reducers } from './reducers';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const config = {
  storage,
  key: 'root',
};

const logger = createLogger({
  collapsed: true,
});

const persistedReducer = persistReducer(config, reducers);

const createDevelopmentStore = () => {
  const store = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleware, logger),
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export const store = createDevelopmentStore();
